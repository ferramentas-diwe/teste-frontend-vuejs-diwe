# Teste para desenvolvedor Frontend Vue.js

Desenvolver uma pequena aplicação utilizando o Vue.js 

## Premissas

- Utilizar o Vue.js no frontend
- Utilizar o Vuex quando necessário
- Separar em componentes os elementos da aplicação

## Diferenciais
- Documentação dos componentes
- Utilizar o Vuetify ou Quasar framework
- Código limpo
- Modulariadade do código

## Objetivo
O objetivo é construir um painel administrativo utilizando o Vue.js para gerenciar usuários, roles e empresas. O arquivo db.json será utilizado para mockar a API com o JSON SERVER, mais detalhes no fim do documento

## O Desafio
João tem um sistema de gerenciamento empresarial, os usuários do sistema são separados por ROLES e cada usuário é funcionário de uma empresa do sistema.

João ainda não possui um painel administrativo para gerenciar esses usuários.

João quer cadastrar / editar as roles do seu sistema, as empresas e os usuários. Ele quer que isto seja feito da forma mais fácil possível, onde ele mesmo possa selecionar qual a role do usuário e qual a empresa que este usuário faz parte, ele ainda não tem uma ideia de layout e não sabe qual a melhor forma de fazer isto.

Seu objetivo é ajuda-lo e resolver o problema.

## Pontos de atenção

A arquitetura e o layout é por sua conta seja esperto.

Utilize o Eslint para auxiliar você com os padrões.

Leia atentamente a documentação do JSON SERVER, ela vai te ajudar com filters, search e paginate.

https://github.com/typicode/json-server

## O que vamos avaliar?
1. Funcionamento e método de resolução do problema.
2. Organização do código.
3. Performance do código.
4. Documentação.
5. Organização dos componentes
6. Semântica, estrutura, legibilidade, manutenibilidade, escalabilidade do seu código e suas tomadas de decisões.
7. Gerenciamento de estado
8. Históricos de commits do git.

## Entrega

Você deve enviar o link do repositório PUBLICO para o endereço de e-mail: 

**mateus.silva@diwe.com.br CC: maicon.passos@diwe.com.br**

# ANEXO 1 - Instruções de instalação da API Mock - JSON SERVER

Install JSON Server

`npm install -g json-server`

**Utilize o db.json que está neste repositório**

Start JSON Server

`json-server --watch db.json`

Now if you go to http://localhost:3000/users?_expand=company&_expand=role 
